INCLUDE Irvine32.inc
main     EQU start@0
NULL                                 equ 0
SND_ASYNC                            equ 1h
SND_FILENAME                         equ 20000h
ExitProcess PROTO STDCALL :DWORD
BUFFER_SIZE = (29*24)
boardWidth = 23

upKey = 4800h
rightKey = 4D00h
downKey = 5000h
leftKey = 4B00h

maxScore = 17300

.data
	;gameboard
		buffer BYTE BUFFER_SIZE DUP(?)
		fileHandle HANDLE ?
		mapLen DWORD 575
		theMap BYTE 575 DUP(?)
		Map db 0DAh, 0C4h, 0C4h, 0C4h, 0C4h, 0C4h, 0C4h, 0C4h, 0C4h, 0C4h,0BFh, 0DAh, 0C4h, 0C4h, 0C4h, 0C4h, 0C4h, 0C4h, 0C4h, 0C4h, 0C4h,0BFh, 0
			db 0B3h, 2Eh, 2Eh, 2Eh, 2Eh, 2Eh, 2Eh, 2Eh, 2Eh, 2Eh, 0B3h, 0B3h, 2Eh, 2Eh, 2Eh, 2Eh, 2Eh, 2Eh, 2Eh, 2Eh, 2Eh, 0B3h, 0
			db 0B3h, 2Eh, 0DAh, 0C4h, 0BFh, 2Eh, 0DAh, 0C4h, 0BFh, 2Eh, 2 DUP(0B3h),2Eh, 0DAh, 0C4h, 0BFh,2Eh, 0DAh, 0C4h, 0BFh,2Eh, 0B3h, 0
			db 0B3h, 2Eh,0C0h, 0C4h, 0D9h,2Eh, 0C0h, 0C4h, 0D9h,2Eh, 0C0h, 0D9h,2Eh, 0C0h, 0C4h, 0D9h,2Eh, 0C0h, 0C4h, 0D9h,2Eh, 0B3h, 0
			db 0B3h, 20 DUP(2Eh), 0B3h, 0
			db 0B3h, 2Eh,0DAh, 0BFh,2Eh, 0DAh, 0BFh,2Eh, 0DAh, 4 DUP(0C4h), 0BFh,2Eh, 0DAh, 0BFh,2Eh, 0DAh, 0BFh,2Eh, 0B3h,0
			db 0B3h, 2Eh,0C0h, 0D9h,2Eh, 0C0h, 0D9h,2Eh, 0C0h, 0C4h, 0BFh, 0DAh, 0C4h, 0D9h,2Eh, 0C0h, 0D9h,2Eh, 0C0h, 0D9h,2Eh, 0B3h,0
			db 0B3h, 9 DUP(2Eh), 2 DUP(0B3h),9 DUP(2Eh),0B3h, 0
			db 0C0h, 2 DUP(0C4h), 0BFh,02Eh, 0DAh, 2 DUP(0C4h), 2 DUP(' '), 0C0h, 0D9h, 2 DUP(' '),   2 DUP(0C4h), 0BFh,02Eh, 0DAh, 2 DUP(0C4h), 0D9h, 0
			db 3 DUP(' '), 0B3h, 02Eh, 0B3h, 10 DUP(' '), 0B3h, 02Eh, 0B3h, 3 DUP(' '), 0
			db 3 DUP(' '), 0B3h, 02Eh, 0B3h, ' ' , 0DAh, 0C4h, 4 DUP(' '),  0C4h, 0BFh, ' ' , 0B3h, 02Eh, 0B3h, 3 DUP(' '), 0
			db 3 DUP(' '), 0B3h, 02Eh, 0B3h, ' ' , 0B3h, 6 DUP(' '), 0B3h, ' ' , 0B3h, 02Eh, 0B3h, 3 DUP(' '), 0
			db 3 DUP(' '), 0B3h, 02Eh, 2 DUP(' '), 0B3h, 6 DUP(' '), 0B3h, 2 DUP(' '), 02Eh, 0B3h, 3 DUP(' '), 0
			db 3 DUP(' '), 0B3h, 02Eh, 0B3h, ' ', 0B3h, 6 DUP(' '), 0B3h, ' ' , 0B3h, 02Eh, 0B3h, 3 DUP(' '), 0
			db 3 DUP(' '), 0B3h, 02Eh, 0B3h, ' ' , 0C0h, 6 DUP(0C4h), 0D9h, ' ', 0B3h, 02Eh, 0B3h, 3 DUP(' '),0
			db 3 DUP(' '), 0B3h, 02Eh, 0B3h, 10 DUP(' '), 0B3h, 02Eh, 0B3h, 3 DUP(' '), 0
			db 0DAh, 2 DUP(0C4h), 0D9h,2Eh, 0C0h, 10 DUP(0C4h), 0D9h,2Eh, 0C0h, 2 DUP(0C4h), 0BFh, 0
			db 0B3h, 20 DUP(2Eh), 0B3h, 0
			db 0B3h, 2Eh, 0DAh, 0BFh, 4 DUP(2Eh), 0DAh, 4 DUP(0C4h), 0BFh, 4 DUP(2Eh), 0DAh, 2 DUP(0C4h), 0D9h, 0
			db 0B3h, 2Eh, 0C0h, 0D9h,2Eh, 0DAh, 0BFh,2Eh, 0C0h, 0C4h, 0BFh, 0DAh, 0C4h, 0D9h,2Eh, 0DAh, 0BFh,2Eh, 0C0h, 2 DUP(0C4h), 0BFh, 0
			db 0B3h, 4 DUP(2Eh), 2 DUP(0B3h),3 DUP(2Eh), 2 DUP(0B3h),3 DUP(2Eh), 2 DUP(0B3h),4 DUP(2Eh), 0B3h, 0
			db 0B3h, 2Eh, 0DAh, 2 DUP(0C4h), 0D9h, 0C0h, 0C4h, 0BFh,2Eh, 2 DUP(0B3h),2Eh, 0DAh, 0C4h, 0D9h, 0C0h, 2 DUP(0C4h), 0BFh,2Eh, 0B3h, 0
			db 0B3h, 2Eh, 0C0h, 5 DUP(0C4h), 0D9h,2Eh, 0C0h, 0D9h,2Eh, 0C0h, 5 DUP(0C4h), 0D9h,2Eh, 0B3h, 0
			db 0B3h, 20 DUP(2Eh), 0B3h, 0
			db 0C0h, 20 DUP(0C4h), 0D9h, 0
	
	;game strings
		scoreString  BYTE "Score: ",0
		levelString  BYTE "Level: ",0
		wincaption  BYTE "WINNER!",0
		winnermsg  BYTE "Congrats! You have beaten all 3 levels. You are Pac Man Camp.",0

		MessBoxTitle db "Level Complete",0
		MessBox db "Level Complete: Do you wish to continute?",0 

		MessBoxTitle1 db "Fail",0
		MessBox1 db "You have been eaten by a ghost. Do you wish to restart?", 0 
	
	;game variables
		nextTick DWORD 0
		lengthOfFrame dd 150
		lastKeyPressed dw 4B00h
		score dd 0
		level dd 1

	;character variable
		x byte 1
		y byte 1
		intendedX byte 2
		intendedY byte 1

	;ghost variable
		
		mX byte 20
		mY byte 23
		mIntendedX byte 20
		mIntendedY byte 22
		mDirection word 0

		m1X byte 20
		m1Y byte 1
		m1IntendedX byte 19
		m1IntendedY byte 1
		m1Direction word 3

		m2X byte 1
		m2Y byte 23
		m2IntendedX byte 2
		m2IntendedY byte 23
		m2Direction word 2


	;splashScreen Variables
		buffer1 BYTE BUFFER_SIZE DUP(?)
		directionfile BYTE "DirectionFile.txt", 0
		splashfile BYTE "asciipacmanart.txt",0

		Welcome db "Welcome To the Pac Man Game!", 0
		NextStep db "Begin Playing (1) or Need Directions (2)? ", 0
		directionContinue db "Now that you read the directions would you like to play? Yes (1) or No (2)?: ",0
		invalidnumber db "Invalid Number, Please enter either 1 or 2", 0

		inputnumber dd 0
		directionumber dd 0


.code

;Main Game Logic & Loop
;---------------------------------------------------------------------------
main PROC
	call randomize
	call clrscr
	call splashscreen
	exit
main ENDP

gameLoop proc
	frameStart:
		call getMSeconds
		add eax, lengthOfFrame	;add length of frame to current time
		mov nextTick, eax
		call handleKey
		call moveCharacter
		call ghostUpdate
		call ghost1Update
		call ghost2Update
		call checkScore

		keyboardLoop:
			call getKeyStroke

			call getMSeconds	
			cmp eax, nextTick	;if length of frame has passed jump to frame start

			jl keyboardLoop	;start loop again if framelength hasnt passed

			jmp frameStart		;if length of frame has passed jump to frame start
		;end of keyboardLoop

	
gameLoop endp

;---------------------------------------------------------------------------
;User Input

getKeyStroke proc
	call readkey
	jz noKeyPress	;dont store anything if no key was pressed (readkey returns 0 if no key pressed)
		mov lastKeyPressed, ax	;key pressed, store value in variable
	noKeyPress:
	ret
getKeyStroke endp

handleKey proc
	mov eax, 0
	mov ax, lastKeyPressed	;takes last keypress

	mov cl, x
	mov ch, y

	;determine what keys was pressed
	mov bx, upkey
	cmp bx, ax
	jz up

	mov bx, rightkey
	cmp bx, ax
	jz right
	
	mov bx, downKey
	cmp bx, ax
	jz down

	mov bx, leftKey
	cmp bx, ax
	jz left

	up:
		dec ch
		jmp endOf

	right:
		inc cl
		jmp endOf

	down:
		inc ch
		jmp endOf

	left:
		dec cl
		jmp endOf

	endOf:

	mov intendedX, cl
	mov intendedY, ch

	ret
handleKey endp

;---------------------------------------------------------------------------
;Move Pacman

moveCharacter proc
	;takes intended next position in intendedX, intendedY
	mov eax, 0

	mov al, intendedX
	mov ah, intendedY

	call readarray	;reads array to determine material of next intended position. Returns char in al

	mov bl, ' '
	cmp al, bl
	jz free

	mov bl, '.'
	cmp al, bl
	jz dot

	mov bl, 'O'
	cmp al, bl
	je ghost
	jmp wall

	free:
		call movePacMan
		ret
	dot:
		call movePacMan
		mov eax, score
		add eax, 100
		mov score, eax
		call updateScore
		
		
		ret
	wall:
		ret
	ghost:
		call endGame
		ret
	ret
moveCharacter endp

movePacMan proc
	;write pacman to array at intended x and y
	mov al, intendedX
	mov ah, intendedY
	mov bl, 02h
	call writeToArray

	;clear last position in array
	mov al, x
	mov ah, y
	mov bl, ' '
	call writeToArray

	;move pacman to intended x and y 
	mov dl, intendedX
	mov dh, intendedY
	mov al, 02h
	mov ebx, 14
	call writeToScreen	;draw pacman

	;clear last position
	mov dl, x
	mov dh, y
	mov al, " "
	call writeToScreen	

	;update to the new position
	mov al, intendedX
	mov ah, intendedY
	mov x, al
	mov y, ah

	ret
movePacMan endp

writeToScreen proc
	;dl-> x, dh-> y
	;al-> the char to draw
	;ebx-> text color
	call gotoxy
	push ax
	mov eax, ebx
	call SetTextColor
	pop ax
	call WriteChar
	ret
writeToScreen endp

;---------------------------------------------------------------------------
;Game States***

checkScore proc
	cmp score, maxScore
	je levelComplete
	ret

	levelComplete:
		;print message asking user if they want to move to next level
		INVOKE MessageBox, NULL, ADDR Messbox,
		ADDR MessBoxTitle, MB_YESNO + MB_ICONQUESTION
		cmp eax, IDYES
		je increaselevel	;increase level if they say yes
		;otherwise exit
		call resetBoard
		call reinitGhosts
		call splashScreen
		mov lengthOfFrame, 150
	ret

	increaselevel:
		call newLevel
	ret

checkScore endp

newLevel PROC
	;check if last level
	mov eax, level
	cmp eax, 3
	je levelscomplete
	jmp nextLevel


	levelscomplete:	;if complete all levels
		mov ebx,OFFSET wincaption
		mov edx,OFFSET winnermsg
		call MsgBox
		
	ret

	nextLevel:
		mov score,0 ;puts score back at zero for the next level
		inc level
		mov eax, 20
		sub lengthOfFrame, eax	;increase pacman and ghosts' speed 
		call resetBoard
	ret

newLevel ENDP

endGame PROC
	INVOKE MessageBox, NULL, ADDR Messbox1,
	ADDR MessBoxTitle1, MB_YESNO + MB_ICONQUESTION
	cmp eax, IDYES
	jnz startScreen
	restartGamee:
		call restartGame
		jmp endOf
	startScreen:
		call restartGame
		call splashScreen
		mov lengthOfFrame, 150
		
	endOf:
	ret
endGame ENDP

restartGame PROC
	mov score, 0 ;puts score back at zero for the beginning level
	mov level, 1
	call resetBoard
	call reinitGhosts
	ret
restartGame endp

resetBoard PROC
	;set variables to the initial value
	mov score, 0
	mov x, 1
	mov y, 1
	mov intendedX, 2
	mov intendedY, 1
	mov lastKeyPressed, 4D00
	mov eax, 15 ;changes color back to normal
	call SetTextColor
	call clrscr
	call drawscreen
	call updateScore

	;put pacman to the initial position
	mov dl, x
	mov dh, y
	mov al, 02h
	call writeToScreen	;draw pacman

	ret
resetBoard endp

;---------------------------------------------------------------------------
;Ghost Logic

reinitGhosts PROC
	;put ghosts to the initial position
	mov mX, 20
	mov mY, 23
	mov mIntendedX, 20
	mov mIntendedY, 22

	mov m1X, 20
	mov m1Y, 1
	mov m1IntendedX, 19
	mov m1IntendedY, 1

	mov m2X, 1
	mov m2Y, 23
	mov m2IntendedX, 2
	mov m2IntendedY, 23

	ret
reinitGhosts endp

ghostUpdate PROC
	;Re-decide the direction every time
	;the direction is according to a random number between one and four
	mov eax, 4
	Call RandomRange
	mov mDirection, ax
start:	
	mov cl, mX
	mov ch, mY

	mov bx, 0
	cmp bx, ax
	jz up

	mov bx, 1
	cmp bx, ax
	jz right
	
	mov bx, 2
	cmp bx, ax
	jz down

	mov bx, 3
	cmp bx, ax
	jz left

	up:
		dec ch
		jmp endOf
	right:
		inc cl
		jmp endOf
	down:
		inc ch
		jmp endOf
	left:
		dec cl
		jmp endOf
	endOf:

	mov  mIntendedX, cl
	mov  mIntendedY, ch

	mov al, cl
	mov ah, ch
	call readarray	;reads array to determine material of next intended position. Returns char in al

	mov bl, ' '
	cmp al, bl
	jz free

	mov bl, '.'
	cmp al, bl
	jz free

	mov bl, 02h
	cmp al, bl
	jz pacman

	
	jmp wall

	free:
		call moveGhost
		ret
	wall:
		mov eax, 4
		Call RandomRange
		mov mDirection, ax
		jmp start
	pacman:
		call endGame
		ret
ghostUpdate endp


moveGhost PROC
		;read the character in current position
		mov al, mX
		mov ah, mY
		call readArray

		;restore the character in current position
		mov dl, mX
		mov dh, mY
		mov ebx, 15
		call writeToScreen	

		;move ghost to intended x and y on the screen
		mov dl, mIntendedX
		mov dh, mIntendedY
		mov al, "O"
		mov ebx, white + (red * 16)	;set the ghost a white O with red background
		call writeToScreen	

		;update to the new position
		mov mX, dl
		mov mY, dh
	ret
moveGhost endp

ghost1Update PROC
	;Re-decide the direction every time
	;the direction is according to a random number between one and four
	mov eax, 4
	Call RandomRange
	mov m1Direction, ax
start:
	mov cl, m1X
	mov ch, m1Y

	mov bx, 0
	cmp bx, ax
	jz up

	mov bx, 1
	cmp bx, ax
	jz right
	
	mov bx, 2
	cmp bx, ax
	jz down

	mov bx, 3
	cmp bx, ax
	jz left

	up:
		dec ch
		jmp endOf
	right:
		inc cl
		jmp endOf
	down:
		inc ch
		jmp endOf
	left:
		dec cl
		jmp endOf
	endOf:

	mov  m1IntendedX, cl
	mov  m1IntendedY, ch

	mov al, cl
	mov ah, ch
	call readarray	;reads array to determine material of next intended position. Returns char in al

	mov bl, ' '
	cmp al, bl
	jz free

	mov bl, '.'
	cmp al, bl
	jz free

	mov bl, 02h
	cmp al, bl
	jz pacman

	jmp wall

	free:
		call move1Ghost
		ret
	wall:
		mov eax, 4
		Call RandomRange
		mov m1Direction, ax
		jmp start
	pacman:
		call endGame
		ret
ghost1Update endp

move1Ghost PROC
		;read the character in current position
		mov al, m1X
		mov ah, m1Y
		call readArray

		;restore the character in current position
		mov dl, m1X
		mov dh, m1Y
		mov ebx, 15
		call writeToScreen

		;move ghost to intended x and y on the screen
		mov dl, m1IntendedX
		mov dh, m1IntendedY
		mov al, "O"
		mov ebx, white + (cyan * 16)	;set the ghost1 a white O with cyan background
		call writeToScreen

		;update to the new position
		mov m1X, dl	;update cordinates
		mov m1Y, dh
	ret
move1Ghost endp

ghost2Update PROC
	;Re-decide the direction every time
	;the direction is according to a random number between one and four
	mov eax, 4
	Call RandomRange
	mov m2Direction, ax
start:
	mov cl, m2X
	mov ch, m2Y

	mov bx, 0
	cmp bx, ax
	jz up

	mov bx, 1
	cmp bx, ax
	jz right
	
	mov bx, 2
	cmp bx, ax
	jz down

	mov bx, 3
	cmp bx, ax
	jz left

	up:
		dec ch
		jmp endOf
	right:
		inc cl
		jmp endOf
	down:
		inc ch
		jmp endOf
	left:
		dec cl
		jmp endOf
	endOf:

	mov  m2IntendedX, cl
	mov  m2IntendedY, ch

	mov al, cl
	mov ah, ch
	call readarray	;reads array to determine material of next intended position. Returns char in al

	mov bl, ' '
	cmp al, bl
	jz free

	mov bl, '.'
	cmp al, bl
	jz free

	mov bl, 02h
	cmp al, bl
	jz pacman

	jmp wall

	free:
		call move2Ghost
		ret
	wall:
		mov eax, 4
		Call RandomRange
		mov m2Direction, ax
		jmp start
	pacman:
		call endGame
		ret
ghost2Update endp

move2Ghost PROC
		;read the character in current position
		mov al, m2X
		mov ah, m2Y
		call readArray

		;restore the character in current position
		mov dl, m2X
		mov dh, m2Y
		mov ebx, 15
		call writeToScreen
		
		;move ghost2 to intended x and y on the screen
		mov dl, m2IntendedX
		mov dh, m2IntendedY
		mov al, "O"
		mov ebx, white + (magenta * 16)	;set the ghost a white O with magenta background
		call writeToScreen

		;update to the new position
		mov m2X, dl	;update cordinates
		mov m2Y, dh
	ret
move2Ghost endp

;---------------------------------------------------------------------------
;Gameboard Loading, Displaying & Score

LoadGameBoard PROC
	;copy each character from Map to theMap
	mov ecx, mapLen
	mov esi, OFFSET Map
	mov edi, OFFSET theMap
	cld
	rep movsb
	ret
LoadGameBoard ENDP

drawScreen proc
	call LoadGameBoard
	mov ecx, mapLen
	mov esi, 0
	printcharacters:
		mov bl, theMap[esi]
		cmp bl, 2Eh
		je changecolor
		cmp bl, 0
		je newline
		jmp somewhere

		changecolor:
			mov eax, 15
			call SetTextColor
			mov al,theMap[esi]
			call WriteChar
			jmp keepgoing
		newline:
			;if the char is 0, print nextline
			call Crlf
			jmp keepgoing
		somewhere:
			;if the char is drawing character, then set color to blue
			mov eax, 1
			call SetTextColor
			mov al, theMap[esi]
			call WriteChar
		keepgoing:
			inc esi
	loop printcharacters

	;print pacman on the screen
	mov dl, x
	mov dh, y
	mov al, 02h
	mov ebx, 14
	call writeToScreen

	call updateScore
	ret
drawScreen endp

updateScore proc
	;score
	mov dl,35
	mov dh, 10
	call gotoxy
	mov edx, offset scoreString
	call WriteString
	mov eax, score
	call writedec

	;level
	mov dl, 35
	mov dh, 8
	call gotoxy
	mov edx, offset levelString
	call WriteString
	mov eax, level
	call writedec

	ret		
updateScore endp

;---------------------------------------------------------------------------
;Gameboard Array Procedures

readArray proc
	;al: x, ah: y
	;returns value in al
	mov esi, offset theMap
	mov ecx, eax
	mov eax, 0	;use ax to store position
	;determine position
	mov al, boardWidth
	mul ch
	;ax = boardwidth * y
	mov ch, 0	;make cx equal to cl only
	add ax, cx	;add the x to the sum. Ax is now the offset from the begining of the array
	add esi, eax; add the offset off the array to its position in the array
	mov al, [esi]
	ret
readArray endp

writeToArray proc
	;al: x, ah: y
	;bl char to write
	mov esi, offset theMap
	mov ecx, eax
	mov eax, 0	;use ax to store position
	;determine position
	mov al, boardWidth
	mul ch
	;ax = boardwidth * y
	mov ch, 0	;make cx equal to cl only
	add ax, cx	;add the x to the sum. Ax is now the offset from the begining of the array
	add esi, eax; add the offset off the array to its position in the array
	mov [esi], bl
	ret
writeToArray endp

;-------------------------------------------------------------------------------------------
;Splash Screen
splashscreen PROC

	call clrscr

	mov edx, 0
	mov dh, 7
	call Gotoxy
	mov edx, Offset splashfile
	call openinputfile
	mov filehandle, eax
	mov edx, offset buffer1
	mov ecx, buffer_size
	call ReadFromFile
	mov buffer1[eax],0
	mov edx, offset buffer1
	mov eax,lightblue
	call SetTextColor
	call writestring
	call crlf
	mov eax, filehandle
	call closefile

	mov edx, 0
	mov dh, 15
	call Gotoxy
	mov edx, Offset splashfile
	call openinputfile
	mov filehandle, eax
	mov edx, offset buffer1
	mov ecx, buffer_size
	call ReadFromFile
	mov buffer1[eax],0
	mov edx, offset buffer1
	mov eax,lightblue
	call SetTextColor
	call writestring
	call crlf
	mov eax, filehandle
	call closefile


	mov edx, 0
	mov dh, 12
	mov dl, 30
	call Gotoxy
	mov edx, offset Welcome
	mov eax,lightGreen
	call SetTextColor
	call WriteString

	call crlf
	mov dh, 14
	mov dl,23
	call Gotoxy
	mov edx, offset NextStep
	mov eax,cyan
	call SetTextColor
	call WriteString
	call Readint
	mov inputnumber, eax

	mov eax, 15 ;changes color back to normal
	call SetTextColor

	cmp inputnumber, 1
	je beginplaying
	cmp inputnumber, 2
	je directions
	jmp invalidnum

	beginplaying:
		call clrscr
		call drawscreen
		call gameLoop
		call crlf
		ret

	directions:
		call clrscr
		call printdirections

		mov edx, offset directionContinue
		call WriteString
		call Readint
		mov directionumber, eax

		cmp directionumber, 1
		je beginplaying
		cmp directionumber, 2
		je quitgame
		jmp invalidnum

		quitgame:
			exit
		ret

	invalidnum:
		call clrscr
		mov dh, 19
		mov dl, 24
		call Gotoxy
		mov edx, offset invalidnumber
		call WriteString
		call splashscreen
		call crlf

		ret

splashscreen ENDP

printdirections PROC
	
	mov edx, Offset directionfile
	call openinputfile
	mov filehandle, eax
	mov edx, offset buffer
	mov ecx, buffer_size
	call ReadFromFile
	mov buffer[eax],0
	mov edx, offset buffer
	call writestring
	call crlf
	mov eax, filehandle
	call closefile

ret
printdirections endp

END main